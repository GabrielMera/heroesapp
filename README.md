
# Heroesapp
Gustavo Gabriel Mera

Funciones:

• Consultar todos los súper héroes.

• Consultar un único súper héroe por id.

• Consultar todos los súper héroes que contienen, en su nombre, el valor de un parámetro enviado en la petición. 

    Por ejemplo, si enviamos “man” devolverá “Spiderman”, “Superman”, “Manolito el fuerte”, etc.

• Modificar un súper héroe.

• Eliminar un súper héroe.

• Directiva en input set Mayusculas.

# Branchs Feature

• Master : HeroeService con data local

• Feature/MockBackend : HeroeService con peticiones HTTP. Interceptor para simular backend. Loader en peticiones HTTP.


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

