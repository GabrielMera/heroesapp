import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public readonly title: string = 'Heroes App';
  public readonly author: string = 'Gustavo Gabriel Mera';
  public readonly linkedIn: string = 'https://www.linkedin.com/in/gabriel-gustavo-mera-7b6456138/';
}
