import { Component, DebugElement, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, NgControl } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { SharedModule } from '../shared.module';
import { UpperCaseInputDirective } from './upper-case-input.directive';

@Component({
  template: `<input type="text" toUpperCase>`
})

class TestUpperCaseComponent {
  @ViewChild(UpperCaseInputDirective) directive: UpperCaseInputDirective;
}

describe('Directive: UpperCaseInputDirective', () => {
  let component: TestUpperCaseComponent;
  let fixture: ComponentFixture<TestUpperCaseComponent>;
  let inputEl: DebugElement;

  beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
              TestUpperCaseComponent, 
              UpperCaseInputDirective
            ],
            imports: [
              SharedModule
            ],
            providers: [
              {
                provide: NgControl,
                useClass: class extends NgControl {
                  control = new FormControl();
                  viewToModelUpdate() {}
                }
              }
            ]
        });
        fixture = TestBed.createComponent(TestUpperCaseComponent);
        component = fixture.componentInstance;
        inputEl = fixture.debugElement.query(By.directive(UpperCaseInputDirective));
        fixture.detectChanges();
    });

    it('should create an instance', () => {
      fixture.detectChanges();
      expect(component.directive).toBeTruthy();
    });

    it('should uppercase lowerCasetext', ()=> {
      component.directive.ngControl.control?.setValue('abcdef');
      fixture.detectChanges();
      expect( component.directive.ngControl.control?.value).toBe('ABCDEF');
    })

    it('should uppercase TitleCasetext', ()=> {
      component.directive.ngControl.control?.setValue('Abcdef');
      fixture.detectChanges();
      expect( component.directive.ngControl.control?.value).toBe('ABCDEF');
    })

    it('should uppercase text with numbers', ()=> {
      component.directive.ngControl.control?.setValue('AbCdEf5');
      fixture.detectChanges();
      expect( component.directive.ngControl.control?.value).toBe('ABCDEF5');
    })

    afterEach(() => {
      if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
        (fixture.nativeElement as HTMLElement).remove();
      }
    });

});
