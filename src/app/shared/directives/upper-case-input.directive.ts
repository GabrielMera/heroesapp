import { Directive } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[toUpperCase]'
})
export class UpperCaseInputDirective {
  valueChangesSubscription: Subscription = new Subscription();

  constructor(public ngControl: NgControl) {}
  ngOnInit(): void {
    const abstractControl = this.ngControl.control;
     this.valueChangesSubscription = abstractControl!.valueChanges
       .subscribe(
         (text: string) => {
           const textUpperCase = text.toUpperCase();
           this.ngControl.control?.setValue(textUpperCase, { emitEvent: false });
         }
       );
   }
 
   ngOnDestroy(): void{
     this.valueChangesSubscription.unsubscribe()
   }

}
