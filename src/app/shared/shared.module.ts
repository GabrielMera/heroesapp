import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogComponent } from './components/dialog/dialog.component';
import { MatDialogModule } from "@angular/material/dialog";
import { MatButtonModule } from '@angular/material/button';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { UpperCaseInputDirective } from './directives/upper-case-input.directive';



@NgModule({
  declarations: [
    DialogComponent,
    FooterComponent,
    HeaderComponent,
    UpperCaseInputDirective
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatToolbarModule
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    UpperCaseInputDirective
  ]
})
export class SharedModule { }
