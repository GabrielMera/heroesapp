import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

interface configDialog{
  title: string,
  okText: string,
  cancelText: string,
  message: string
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})

export class DialogComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public config: configDialog) {

  }

  ngOnInit(): void {
  }

  close(): void {
    this.dialogRef.close(false);
  }
  save(): void{
    this.dialogRef.close(true);
  }

}


