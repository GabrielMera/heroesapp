import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroeAddEditComponent } from './pages/add-edit/add-edit.component';
import { HeroeListComponent } from './pages/list/list.component';
import { HeroeViewComponent } from './pages/view/view.component';


const routes: Routes = [
   { path: '', component: HeroeListComponent },
   { path: ':id/view', component: HeroeViewComponent },
   { path: ':id/:action', component: HeroeAddEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeroesRoutingModule { }