import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroeListComponent } from './pages/list/list.component';
import { HeroeTableComponent } from './pages/list/components/heroe-table/heroe-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { HeroesRoutingModule } from './heroes-routing.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { SharedModule } from '../shared/shared.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { HeroeFilterPanelComponent } from './pages/list/components/heroe-filter-panel/heroe-filter-panel.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { HeroeViewComponent } from './pages/view/view.component';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { HeroeAddEditComponent } from './pages/add-edit/add-edit.component';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  declarations: [
    HeroeListComponent,
    HeroeTableComponent,
    HeroeFilterPanelComponent,
    HeroeViewComponent,
    HeroeAddEditComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    HeroesRoutingModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatDividerModule,
    MatSelectModule
  ]
})
export class HeroesModule { }
