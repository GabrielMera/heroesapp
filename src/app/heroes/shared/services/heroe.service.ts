import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MemoryDataHelper } from '../helpers/memoryData.helper';
import { Editorial } from '../models/editorial.model';
import { Heroe } from '../models/heroe.model';

@Injectable({
  providedIn: 'root'
})
export class HeroeService {

  private readonly apiUrl = environment.api;
  
  constructor(public http: HttpClient) {
  }

  public getHeroes(): Observable<Heroe[]>{
    return this.http.get<Heroe[]>( `${this.apiUrl}/heroes`, {});
  }

  public createHeroe(heroe: Heroe): Observable<Heroe>{
    return this.http.post<Heroe>( `${this.apiUrl}/heroes`, heroe);
  }

  public updateHeroe(heroe: Heroe): Observable<Heroe>{
    return this.http.put<Heroe>( `${this.apiUrl}/heroes/${heroe.id}`, heroe);
  }

  public deleteHeroe(id: number): Observable<void>{
    return this.http.delete<void>( `${this.apiUrl}/heroes/${id}`);
  }

  public getHeroe(id: number): Observable<Heroe>{
    return this.http.get<Heroe>( `${this.apiUrl}/heroes/${id}`, {});
  }

  public searchHeroes(query: string): Observable<Heroe[]>{
    return this.http.get<Heroe[]>( `${this.apiUrl}/heroes`, {params: {query}});
  }

  public getEditoriales(): Observable<Editorial[]>{
    return this.http.get<Editorial[]>( `${this.apiUrl}/editoriales`, {});
  }
 
}
