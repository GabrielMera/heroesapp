import { TestBed } from '@angular/core/testing';
import { HEROES } from 'src/app/core/mock/heroes-mock';
import { Heroe } from '../models/heroe.model';
import { HeroeService } from './heroe.service';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MemoryDataHelper } from '../helpers/memoryData.helper';

describe('HeroeService', () => {
  let heroeService: HeroeService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        HeroeService,
        HttpClient
      ]
    });
    heroeService = TestBed.inject(HeroeService);
    httpController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(heroeService).toBeTruthy();
  });

  it('should get all heroes', () => {
    heroeService.getHeroes()
      .subscribe(
        (heroes) => {
          expect(heroes).toBe(HEROES);
        }
      );
      const req = httpController.expectOne('api/heroes');
      expect(req.request.method).toEqual('GET');
      const heroes = MemoryDataHelper.getHeroes()
      req.flush(heroes);

  });

  it('should get a heroe by id', () => {
    const heroeTest = HEROES[0];
    const id = heroeTest.id;
    heroeService.getHeroe(id)
      .subscribe(
        (heroe) => {
          expect(heroe).toBe(heroeTest);
        }
      );

    const req = httpController.expectOne(`api/heroes/${id}`);
    expect(req.request.method).toEqual('GET');
    const heroe = MemoryDataHelper.getHeroe(id);
    req.flush(heroe);

  });

  it('should get all heroes that contains the text', () => {
    const text = 'man'; 
    heroeService.searchHeroes(text)
      .subscribe(
        (heroes) => {
          expect(heroes.length).toBe(4);
        }
      );

    const req = httpController.expectOne(`api/heroes?query=${text}`);
    expect(req.request.method).toEqual('GET');
    const heroes = MemoryDataHelper.getHeroes(text);
    req.flush(heroes);

  });

  it('should update a heroe', () => {
    const heroeTest = HEROES[0];
    heroeTest.name = "Aquaman";
    heroeService.updateHeroe(heroeTest)
      .subscribe(
        (heroe) => {
          expect(heroe.name).toBe(heroeTest.name);
        }
      );

    const req = httpController.expectOne(`api/heroes/${heroeTest.id}`);
    expect(req.request.method).toEqual('PUT');
    MemoryDataHelper.updateHeroe(heroeTest)
    req.flush(MemoryDataHelper.getHeroe(heroeTest.id));

  });

  it('should delete a heroe', () => {
    const id = HEROES[0].id;
    heroeService.deleteHeroe(id)
      .subscribe(
        () => {
          expect(HEROES.length).toBe(4);
        }
      );
    
    const req = httpController.expectOne(`api/heroes/${id}`);
    expect(req.request.method).toEqual('DELETE');
    MemoryDataHelper.deleteHeroe(id)
    req.flush(MemoryDataHelper.getHeroes());

  });

  it('should not update a inexistent heroe ', () => {
    const heroeTest: Heroe = {
      id: 25,
      name: "Aquaman",
      description: "Puede respirar bajo el agua",
      idEditorial: 1
    }

    heroeService.updateHeroe(heroeTest)
      .subscribe(
        () => {
         fail("No deberia actualizar heroe inexistente");
        },
        (error: HttpErrorResponse) => {
          expect(error.status).toBe(400);
        }
      );

    const req = httpController.expectOne(`api/heroes/${heroeTest.id}`);
    expect(req.request.method).toEqual('PUT');

    req.flush('error', {status: 400 , statusText: "error"});
  });

  it('should not delete a inexistent heroe ', () => {
    const idInexistent: number = 25;
    heroeService.deleteHeroe(idInexistent)
      .subscribe(
        () => {
         fail("No deberia eliminar heroe inexistente");
        },
        (error: HttpErrorResponse) => {
          expect(error.status).toBe(400);
        }
      );

    const req = httpController.expectOne(`api/heroes/${idInexistent}`);
    expect(req.request.method).toEqual('DELETE');
    MemoryDataHelper.deleteHeroe(idInexistent);
    req.flush('error', {status: 400 , statusText: "error"});
  });

  it('should not get a inexistent heroe ', () => {
    const idInexistent: number = 25;
    heroeService.getHeroe(idInexistent)
      .subscribe(
        () => {
         fail("No deberia eliminar heroe inexistente");
        },
        (error: HttpErrorResponse) => {
          expect(error.status).toBe(400);
        }
      );

    const req = httpController.expectOne(`api/heroes/${idInexistent}`);
    expect(req.request.method).toEqual('GET');
    MemoryDataHelper.getHeroe(idInexistent);
    req.flush('error', {status: 400 , statusText: "error"});
  });

  it('Should create heroe', () => {
    const newHeroe: Heroe = {
      id: 5,
      name: 'Robin',
      idEditorial: 1,
      description: 'amigo de batman'
    };
    heroeService.createHeroe(newHeroe)
      .subscribe(
        (heroe) => {
        expect(heroe).toBeTruthy('No se encontro Heroe');
        expect(heroe?.id).toBe(5,'No se creo Heroe');
        },
        (error)=>{
          fail("No deberia existir error");
        })

    const req = httpController.expectOne(`api/heroes`);
    expect(req.request.method).toEqual('POST');
    const heroe = MemoryDataHelper.createHeroe(newHeroe);
    req.flush(heroe);

  });

});
