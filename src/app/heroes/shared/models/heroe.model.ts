export interface Heroe{
    id: number;
    name: string;
    description: string;
    idEditorial: number;
    editorial?: string;
}