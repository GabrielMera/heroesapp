export interface Editorial {
    id: number;
    name: string;
}