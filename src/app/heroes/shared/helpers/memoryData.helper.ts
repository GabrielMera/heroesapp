import { EDITORIALES } from "src/app/core/mock/editorial-mock";
import { HEROES } from "src/app/core/mock/heroes-mock";
import { Editorial } from "../models/editorial.model";
import { Heroe } from "../models/heroe.model";

export class MemoryDataHelper{
    public static heroes: Heroe[] = HEROES;
    public static editoriales: Editorial[] = EDITORIALES;

    public static getNewId(): number{
        const ids: number[] = this.heroes.map((heroe: Heroe) => heroe.id)
        const newId = Math.max(...ids) + 1;
        return newId;
    }

    public static getIndexHeroe(id: number): number{
        return this.heroes.findIndex(heroeDB => heroeDB.id === id);
    }

    public static getHeroe(id: number): Heroe{
        const index = this.getIndexHeroe(id);
        return this.heroes[index];
    }

    public static getHeroes(filterText: string = ''): Heroe[]{
        if(filterText){
            filterText = filterText.toLowerCase();
            return this.heroes.filter(
                (heroe: Heroe) => heroe.name.toLowerCase().includes(filterText)) ;
        }
        return this.heroes;
    }

    public static createHeroe(heroe: Heroe): Heroe{
        const editorial = this.getEditorial(heroe.idEditorial);
        heroe.id = this.getNewId();
        heroe.editorial = editorial?.name;
        this.heroes.push(heroe);
        return heroe;
    }

    public static deleteHeroe(id: number): void{
        const indexHeroe = this.getIndexHeroe(id);
        this.heroes.splice(indexHeroe, 1);
    }

    public static updateHeroe(heroe: Heroe): void{
        const editorial = this.getEditorial(heroe.idEditorial);
        heroe.editorial = editorial?.name;
        this.heroes.forEach((heroeDB, idx) => {
          if(heroeDB.id === heroe.id){
              this.heroes[idx] = heroe;
          }
        });
    }

    public static getEditorial(id: number): Editorial | undefined{
        const editorial: Editorial | undefined= this.editoriales.find(editorial => editorial.id === id);
        return editorial;
    }

    static getEditoriales(): Editorial[] {
        return this.editoriales;
    }
}