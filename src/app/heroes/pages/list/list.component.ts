import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Heroe } from '../../shared/models/heroe.model';
import { HeroeService } from '../../shared/services/heroe.service';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class HeroeListComponent implements OnInit {
  public heroes: Heroe[] = [];

  constructor(
    private heroeService: HeroeService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.getHeroesList();
  }

  private getHeroesList(): void{
    this.heroeService.getHeroes()
      .subscribe(
        (heroes: Heroe[]) => {
          this.heroes = JSON.parse(JSON.stringify(heroes));
        },
        (error) => {
          console.log(error)
        }
      )
  }

  public deleteHeroe(id: number): void{
    this.heroeService.deleteHeroe(id).subscribe(
      () => {
        this.getHeroesList();
      },
      (error) => {
        console.log(error);
      }
    )
  }

  public searchHeroes(text: string): void{
    if(!text){
      this.getHeroesList();
      return;
    }
    this.heroeService.searchHeroes(text).subscribe(
      (heroes: Heroe[]) => {
        this.heroes = heroes;
      },
      (error)=> {
        console.log(error);
      }
    )
  }

  public viewHeroe(id: number): void{
    this.router.navigate(['heroes', id, 'view']);
  }

  public editHeroe(id: number): void{
    this.router.navigate(['heroes', id, 'edit']);
  }

  public createHeroe(): void{
    this.router.navigate(['heroes', 0, 'add']);
  }


}
