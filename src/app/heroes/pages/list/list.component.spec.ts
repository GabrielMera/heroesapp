import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { HEROES } from 'src/app/core/mock/heroes-mock';
import { HeroesModule } from '../../heroes.module';
import { HeroeService } from '../../shared/services/heroe.service';
import { HeroeListComponent } from './list.component';

describe('ListComponent', () => {
  let component: HeroeListComponent;
  let fixture: ComponentFixture<HeroeListComponent>;
  let element: DebugElement;
  let heroeService: any

  beforeEach(async () => {
    const heroeServiceSpy = jasmine.createSpyObj('HeroeService', ['getHeroes', 'searchHeroes', 'deleteHeroe'])
    await TestBed.configureTestingModule({
      declarations: [ 
        HeroeListComponent
      ],
      imports: [
        RouterTestingModule,
        NoopAnimationsModule,
        HeroesModule
      ],
      providers: [
        { provide: HeroeService, useValue: heroeServiceSpy}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HeroeListComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement;
    heroeService = TestBed.inject(HeroeService);

  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get list heroes', () => {
    heroeService.getHeroes.and.returnValue(of(HEROES));
    fixture.detectChanges();
    const heroes = component.heroes;
    expect(heroes.length).toBe(HEROES.length);
  });

  it('should get heroes by text', () => {
    heroeService.getHeroes.and.returnValue(of(HEROES));
    const text: string = "man";
    const heroesFiltrados = HEROES.filter(heroe => heroe.name.toLowerCase().includes(text))
    heroeService.searchHeroes.and.returnValue(of(heroesFiltrados));
    component.searchHeroes(text);
    expect(component.heroes.length).toBe(heroesFiltrados.length);
  });

  it('should delete heroe by id', () => {
    heroeService.getHeroes.and.returnValue(of(HEROES));
    const id: number = 1;
    const heroesFiltrados = HEROES.filter(heroe => heroe.id != id);
    heroeService.deleteHeroe.and.returnValue(of(heroesFiltrados));
    heroeService.getHeroes.and.returnValue(of(heroesFiltrados));
    component.deleteHeroe(id);
    expect(component.heroes.length).toBe(heroesFiltrados.length);
  });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });


});
