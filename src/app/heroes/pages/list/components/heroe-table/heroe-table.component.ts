import { Component, OnInit, ChangeDetectionStrategy, Input, ViewChild, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Heroe } from 'src/app/heroes/shared/models/heroe.model';
import { DialogComponent } from 'src/app/shared/components/dialog/dialog.component';

@Component({
  selector: 'app-heroe-table',
  templateUrl: './heroe-table.component.html',
  styleUrls: ['./heroe-table.component.scss']
})
export class HeroeTableComponent implements OnChanges {

  public dataSource: MatTableDataSource<Heroe> = new MatTableDataSource<Heroe>() ;
  public displayedColumns: string[] = ['id', 'name', 'editorial', 'actions'];
  
  @Input() 
  heroes: Heroe[];

  @Output()
  heroeDeleted = new EventEmitter<number>();

  @Output()
  onHeroeEdit = new EventEmitter<number>();

  @Output()
  onHeroeView = new EventEmitter<number>();

  @Output()
  onHeroeCreate = new EventEmitter<void>();

  @ViewChild(MatPaginator) 
  paginator: MatPaginator;

  constructor(private dialog: MatDialog) { 
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.heroes = changes.heroes.currentValue;
    this.dataSource = new MatTableDataSource<Heroe>(this.heroes);
    this.dataSource.paginator = this.paginator;
  }

  public handleDelete(id: number, name: string):void{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: "Eliminar Heroe",
      message: `¿Esta seguro que desea eliminar a ${name}?`,
      okText: "Confirmar",
      cancelText: "Cancelar"
    };

    const dialogDelete = this.dialog.open(DialogComponent, dialogConfig);
    dialogDelete.afterClosed()
      .subscribe(
        (confirm: boolean) =>  confirm &&  this.heroeDeleted.emit(id)
      );
  }

  public handleEdit(id: number): void{
    this.onHeroeEdit.emit(id)
  }

  public handleView(id: number): void{
    this.onHeroeView.emit(id)
  }

  public handleCreate(): void{
    this.onHeroeCreate.emit()
  }

}
