import { DebugElement, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HEROES } from 'src/app/core/mock/heroes-mock';
import { HeroesModule } from 'src/app/heroes/heroes.module';

import { HeroeTableComponent } from './heroe-table.component';

describe('HeroeTableComponent', () => {
  let component: HeroeTableComponent;
  let fixture: ComponentFixture<HeroeTableComponent>;
  let element: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeroeTableComponent ],
      imports: [
        HeroesModule,
        NoopAnimationsModule
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HeroeTableComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement;
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display same qty heroes and rows', () => {
    component.ngOnChanges({heroes: new SimpleChange(null, HEROES, true)});
    fixture.detectChanges();
    const filas = element.queryAll(By.css('.mat-row'));
    expect(filas.length).toBe(HEROES.length);
  });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });
});
