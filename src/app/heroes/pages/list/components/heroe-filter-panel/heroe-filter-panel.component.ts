import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-heroe-filter-panel',
  templateUrl: './heroe-filter-panel.component.html',
  styleUrls: ['./heroe-filter-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeroeFilterPanelComponent implements OnInit {
  public filterForm: FormGroup;

  @Output()
  onSearch = new EventEmitter<string>();

  constructor( private fb: FormBuilder) {
    this.filterForm = this.fb.group({
      search: ['']
    })
  }

  ngOnInit(): void {
  }

  public handleSearch(): void {
    const searchText: string = this.filterForm.get('search')?.value;
    this.onSearch.emit(searchText);
  }


}
