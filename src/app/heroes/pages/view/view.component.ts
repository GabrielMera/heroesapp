import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Heroe } from '../../shared/models/heroe.model';
import { HeroeService } from '../../shared/services/heroe.service';

@Component({
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class HeroeViewComponent implements OnInit {
  public heroe: Heroe;
  constructor(
    private route: ActivatedRoute,
    private heroeService: HeroeService) { }

  ngOnInit(): void {
    const heroeId = Number(this.route.snapshot.params['id']);
    this.loadHeroe(heroeId);
  }

  private loadHeroe(id: number): void{
    this.heroeService.getHeroe(id)
      .subscribe(
        (heroe: Heroe) => {
          this.heroe = heroe;
        },
        (error) => {
          console.log(error);
        });
  }

}
