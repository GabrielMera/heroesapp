import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroesModule } from '../../heroes.module';

import { HeroeAddEditComponent } from './add-edit.component';

describe('AddEditComponent', () => {
  let component: HeroeAddEditComponent;
  let fixture: ComponentFixture<HeroeAddEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HeroesModule,
        RouterTestingModule,
        NoopAnimationsModule,
        HttpClientTestingModule
      ],
      declarations: [ 
        HeroeAddEditComponent
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroeAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });
});
