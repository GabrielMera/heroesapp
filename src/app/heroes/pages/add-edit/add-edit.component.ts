import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Editorial } from '../../shared/models/editorial.model';
import { Heroe } from '../../shared/models/heroe.model';
import { HeroeService } from '../../shared/services/heroe.service';

export enum ModeForm {
  add = 0,
  edit = 1
}

@Component({
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class HeroeAddEditComponent implements OnInit {
  public readonly titleCrear: string = 'Crear';
  public readonly titleEditar: string = 'Editar';
  public heroeForm: FormGroup;
  public editoriales: Editorial[] = [];
  public modeForm: ModeForm = ModeForm.add;
  private heroeId: number = 0;
  
  constructor(
    private fb: FormBuilder, 
    private heroService: HeroeService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.heroeForm = this.fb.group({
      name: ['', Validators.required],
      idEditorial:  ['', Validators.required],
      description:  ['', Validators.required]
    })
  }

  ngOnInit(): void {
    this.loadEditoriales();
    const action: ('add' | 'edit') = this.route.snapshot.params['action'];
    this.modeForm = ModeForm[action];
    if(this.modeForm === ModeForm.edit){
      this.heroeId = Number(this.route.snapshot.params['id']);
      this.loadHeroe();
    }
  }

  private loadHeroe(): void{
    this.heroService.getHeroe(this.heroeId)
      .subscribe(
        (heroe: Heroe) => {
        this.heroeForm.patchValue(heroe);
        },
        (error)=> {
          console.log(error);
          this.router.navigate(['/heroes']);
        }
      );
  }

  private loadEditoriales(): void{
    this.heroService.getEditoriales()
      .subscribe(
        (editoriales: Editorial[]) => {
          this.editoriales = editoriales;
        },
        (error) => {
          console.log(error);
        }
      )
  }

  public onSubmit(): void{
    if (!this.heroeForm.valid) return;
    const heroe: Heroe = this.heroeForm.value;

    if(this.modeForm === ModeForm.add){
      this.createHeroe(heroe)
    }else{
      this.updateHeroe(heroe)
    }

  }

  public createHeroe(heroe: Heroe): void{
    this.heroService.createHeroe(heroe)
    .subscribe(
      ()=>{
        this.router.navigate(['/heroes']);
      }, 
      (error) => {
        console.log(error);
      })
  }

  public updateHeroe(heroe: Heroe): void{
    heroe.id = this.heroeId;
    this.heroService.updateHeroe(heroe)
    .subscribe(
      ()=>{
        this.router.navigate(['/heroes']);
      }, 
      (error) => {
        console.log(error);
      })
  }

}
