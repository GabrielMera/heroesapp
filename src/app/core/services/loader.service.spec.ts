import { Overlay, OverlayModule, OverlayRef } from '@angular/cdk/overlay';
import { TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from '../core.module';

import { LoaderService } from './loader.service';

describe('LoaderService', () => {
  let service: LoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        NoopAnimationsModule,
        OverlayModule
      ],
      providers: [
        {
          provide: Overlay,
          useValue:  {}
        },
        {
          provide: OverlayRef,
          useValue:  {}
        }
      ]
    });
    service = TestBed.inject(LoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
