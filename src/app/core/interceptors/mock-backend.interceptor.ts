import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { API } from "../mock/api-server-mock";

@Injectable()
export class MockBakendInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        const currentMockEndpoint = API.find(
            (endpoint) => 
                endpoint.method === request.method && 
                request.urlWithParams.match(endpoint.url)
            );
        if(currentMockEndpoint && environment.mock){ 
            return currentMockEndpoint.callback(request.urlWithParams, request.body)
        }else{
            return next.handle(request);
        }

    }


}