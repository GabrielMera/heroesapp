import { Editorial } from "src/app/heroes/shared/models/editorial.model";

export const EDITORIALES: Editorial[] = [
    { id: 1, name: "DC"},
    { id: 2, name: "Marvel"},
    { id: 3, name: "Power"}
];