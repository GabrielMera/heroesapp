import { Heroe } from "src/app/heroes/shared/models/heroe.model";

export const HEROES: Heroe[] = [
    { id: 1, name: "BATMAN", description: "No posee superpoderes. Multimillonario magnate empresarial", idEditorial:  1, editorial: "DC"},
    { id: 2, name: "SUPERMAN", description: "Periodista y Superheroe", idEditorial: 2, editorial: "Marvel"},
    { id: 3, name: "MUJER MARAVILLA", description: "Superheroina que posee un arsenal de armas.", idEditorial: 2, editorial: "Marvel"},
    { id: 4, name: "ANTMAN", description: "Hombre Hormiga", idEditorial: 2, editorial: "Marvel"},
    { id: 5, name: "SPIDERMAN", description: "Hombre araña", idEditorial: 2, editorial: "Marvel"}
];