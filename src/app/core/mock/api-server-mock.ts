import { HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { Observable, of, throwError } from "rxjs";
import { MemoryDataHelper } from "src/app/heroes/shared/helpers/memoryData.helper";
import { Heroe } from "src/app/heroes/shared/models/heroe.model";
import { delay } from "rxjs/operators";
import { Editorial } from "src/app/heroes/shared/models/editorial.model";

interface mockEndpoint{
    url: RegExp,
    method: string,
    callback(url: string, body: any): any 
}

const getHeroes = (url: string, body: any): Observable<HttpResponse<any>> => {
    const heroes: Heroe[] = MemoryDataHelper.getHeroes();
    return formatHttpResponse(200, heroes);
}

const getHeroesQuery = (url: string, body: any): Observable<HttpResponse<any>> => {
    const query = extractParam(url, 'query');
    const heroes: Heroe[] = MemoryDataHelper.getHeroes(query);
    return formatHttpResponse(200, heroes);
}

const getHeroe = (url: string,  body: any): Observable<HttpResponse<any> | HttpErrorResponse>  =>{
    const id = Number(extractParam(url, 'id'));
    const heroe = MemoryDataHelper.getHeroe(id);
    if(!heroe) return formatHttpErrorResponse(500, 'No Existe Heroe');
    return formatHttpResponse(200, heroe);
}

const deleteHeroe = (url: string,  body: any): Observable<HttpResponse<any>> =>{
    const id = Number(extractParam(url, 'id'));
    MemoryDataHelper.deleteHeroe(id);
    return formatHttpResponse(200, {});
}

const updateHeroe = (url: string,  body: any): Observable<HttpResponse<any>> =>{
    const heroe: Heroe = body;
    MemoryDataHelper.updateHeroe(heroe);
    return formatHttpResponse(200, heroe);
} 

const createHeroe = (url: string,  body: any): Observable<HttpResponse<any>> => {
    const heroe: Heroe = body;
    MemoryDataHelper.createHeroe(heroe);
    return formatHttpResponse(200, heroe);
}

const getEditoriales = (url: string,  body: any): Observable<HttpResponse<any>> => {
    const editoriales: Editorial[] = MemoryDataHelper.getEditoriales();
    return formatHttpResponse(200, editoriales);
}

const formatHttpResponse = (status: number, body: Heroe | Heroe[] | {}): Observable<HttpResponse<any>> =>{
    const response: HttpResponse<any> = new HttpResponse({ status, body });
    return of(response)
            .pipe(delay(600))
}

const formatHttpErrorResponse = (status: number, error: string): Observable<HttpErrorResponse> =>{
    const response: HttpErrorResponse = new HttpErrorResponse({ status, error });
    return throwError(response)
            .pipe(delay(600))
}

const extractParam = (url: string, name: string): string =>{
    if(name === 'id') return url.split("/").pop() || '';
    return url.split(`${name}=`).pop() || '';
}


export const API: mockEndpoint[] = [
    {
        url: new RegExp(/\/heroes+$/),
        method: 'GET',
        callback: getHeroes
    }, 
    {
        url: new RegExp(/\/editoriales/),
        method: 'GET',
        callback: getEditoriales
    },
    {
        url: new RegExp(/\/heroes\/\d+$/),
        method: 'DELETE',
        callback: deleteHeroe
    },
    {
        url: new RegExp(/\/heroes\/\d+$/),
        method: 'GET',
        callback: getHeroe
    },
    {
        url: new RegExp(/\/heroes\?query=\S+$/),
        method: 'GET',
        callback: getHeroesQuery
    },
    {
        url: new RegExp(/\/heroes\/\d+$/),
        method: 'PUT',
        callback: updateHeroe
    },
    {
        url: new RegExp(/\/heroes+$/),
        method: 'POST',
        callback: createHeroe
    }
]



